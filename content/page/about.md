---
title: "About"
date: 2018-03-24T19:55:15-03:00
draft: false
---

Linux Kernel Campinas group study.

## Goal

Our purpose is to level up people to became Linux kernel
developers to a professional level.

## How

There are meetings every Tuesday 19h . The meetings start with
a talk usually about a kernel topic. This talk is
transmitted through stream on the IC channel on YouTube. And
the videos stay available there afterwards. The meetings
are mainly in Portuguese so they are later subtitled in
for English speakers.

a brief explanation usually about a kernel topic. This talk is
transmitted through stream on the
[IC channel](https://www.youtube.com/channel/UCraCE6iWUcFCJSp-vmO1D3A/featured)
on YouTube . And the videos stay available there afterwards.
The meetings are mainly in Portuguese so they are later subtitled
in for English speakers.

After the explanation we dedicated time to work on any interesting
projects. This is the moment we share knowledge by helping each
other solve the problems we encounter. For this we use the two
IRC channels described bellow.

### Subscribe to mailing lists:
- kernelnewbies@kernelnewbies.org  
For 99% of the communication by email
- lkcamp@lists.libreplanetbr.org  
For uses that really doesn’t make sense in kernelnewbies

### Chat channel on IRC:
- #kernelnewbies @ oftc  
  For 99% of the IRC communication
- #lkcamp @ oftc or @ freenode  
For uses that really doesn’t make sense in kernelnewbies
